using Microsoft.Extensions.Options;
using MongoDB.Driver;

using TargetSample.Model;

namespace TargetSample.Repositories
{
    public class ProductPricingRepository : IProductPricingRepository
    {
        private readonly IMongoDatabase _database = null;

        public ProductPricingRepository(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }

        private IMongoCollection<ProductPricing> PricingInfo
        {
            get
            {
                return _database.GetCollection<ProductPricing>("ProductPricing");
            }
        }

        public ProductPricing GetPricing(int productId)
        {
            var filter = Builders<ProductPricing>.Filter.Eq("ProductId", productId);
            var document = PricingInfo.Find(filter).FirstOrDefault();

            if (document == null)
            {
                return new ProductPricing();
            }

            return document;
        }

        public void AddPricing(ProductPricing pricing)
        {
            PricingInfo.InsertOne(pricing);
        }

        // update will upsert record as needed
        public void UpdatePricing(ProductPricing pricing)
        {
            var filter = Builders<ProductPricing>.Filter.Eq("ProductId", pricing.ProductId);
            var update = Builders<ProductPricing>.Update
                           .Set(p => p.Value, pricing.Value)
                           .Set(p => p.CurrencyCode, pricing.CurrencyCode);
            var updateOptions = new UpdateOptions() { IsUpsert = true };
            var result = PricingInfo.UpdateOne(filter, update, updateOptions);
        }
    }
}