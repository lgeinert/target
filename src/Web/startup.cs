using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using TargetSample.Model;
using TargetSample.Services;
using TargetSample.Repositories;

namespace TargetSample
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // add MVC to project
            services.AddMvc();

            // configure mongo connection stuff
            services.Configure<Settings>(options =>
            {
                options.ConnectionString = Configuration.GetSection("MongoConnection:ConnectionString").Value;
                options.Database = Configuration.GetSection("MongoConnection:Database").Value;
            });

            // setup DI stuff
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IWebRequest, WebRequest>();
            services.AddTransient<IProductPricingRepository, ProductPricingRepository>();
            services.AddTransient<IJsonService<ProductServiceResponse>, JsonService<ProductServiceResponse>>();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseMvc();
        }
    }
}
