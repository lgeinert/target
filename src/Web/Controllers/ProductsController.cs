using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TargetSample.Services;
using TargetSample.Model.ViewModel;

namespace TargetSample
{
    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private IProductService _svc;

        public ProductsController(IProductService productService)
        {
            _svc = productService;
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            ProductViewModel result = null;

            try
            {
                result = await _svc.GetProduct(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return StatusCode(500);
            }

            return Ok(result);
        }

        [HttpPut("{id:int}")]
        public IActionResult Put(int id, [FromBody]CurrentPriceViewModel vm)
        {
            try
            {
                _svc.UpdateProductPricing(id, vm);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return StatusCode(500);
            }            
            
            return Ok();
        }
    }
}