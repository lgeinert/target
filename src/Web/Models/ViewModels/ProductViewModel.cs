namespace TargetSample.Model.ViewModel
{
    public class ProductViewModel
    {
        public ProductViewModel ()
        {
            CurrentPrice = new CurrentPriceViewModel();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public CurrentPriceViewModel CurrentPrice { get; set; }

        // build a ProductViewModel from a Product model object & ProductPricing object
        public static ProductViewModel BuildProductViewModel(Product product, ProductPricing pricing)
        {
            var vm = new ProductViewModel();

            vm.Id = product.Item.tcin;
            vm.Name = product.Item.product_description.Title;
            vm.CurrentPrice.Value = pricing.Value;
            vm.CurrentPrice.CurrencyCode = pricing.CurrencyCode;
            
            return vm;
        }
    }
}