namespace TargetSample.Model
{
    public class Item
    {
        public Item ()
        {
          product_description = new ProductDescription();
        }
        
        public int tcin { get; set; }
        public ProductDescription product_description { get; set; }
    }
}