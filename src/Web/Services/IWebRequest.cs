using System.Threading.Tasks;

namespace TargetSample.Services
{
    public interface IWebRequest
    {
        Task<string> GetProduct(string url);
    }
}