using System.Threading.Tasks;
using TargetSample.Model;
using TargetSample.Model.ViewModel;
using TargetSample.Repositories;

namespace TargetSample.Services
{
    public class ProductService : IProductService
    {
        private IWebRequest _request;
        private IJsonService<ProductServiceResponse> _jsonSvc;
        private IProductPricingRepository _pricingRepo;

        private const string ServiceUrlTemplate = "http://redsky.target.com/v1/pdp/tcin/{0}?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics";

        public ProductService(IWebRequest webRequest, IJsonService<ProductServiceResponse> jsonService, IProductPricingRepository productPricingRepository)
        {
            _request = webRequest;
            _jsonSvc = jsonService;
            _pricingRepo = productPricingRepository;
        }

        public async Task<ProductViewModel> GetProduct(int id)
        {
            var url = string.Format(ServiceUrlTemplate, id);
            var stringProduct = await  _request.GetProduct(url);

            // get product data from service, pricing from repository
            var productJson = _jsonSvc.DeserializeObject(stringProduct);
            var productPricing = _pricingRepo.GetPricing(id);

            // build the vm to return
            var vm = ProductViewModel.BuildProductViewModel(productJson.Product, productPricing);

            return vm;
        }

        public void UpdateProductPricing(int id, CurrentPriceViewModel currentPriceVm)
        {
            // map the pricing info
            var pricing = new ProductPricing();
            pricing.ProductId = id;
            pricing.Value = currentPriceVm.Value;
            pricing.CurrencyCode = currentPriceVm.CurrencyCode;

            // update pricing
            _pricingRepo.UpdatePricing(pricing);
        }
    }
}