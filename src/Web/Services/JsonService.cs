using Newtonsoft.Json;

namespace TargetSample.Services
{
    public class JsonService<T> : IJsonService<T>
    {
        public T DeserializeObject(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}